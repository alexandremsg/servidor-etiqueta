FROM maven:3.5-jdk-8 AS build  
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app  
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8u201-jre-alpine3.9
USER root
RUN apk add samba-client
COPY --from=build /usr/src/app/target/servidor-etiqueta-0.1.0.jar /usr/app/servidor-etiqueta-0.1.0.jar  
EXPOSE 8082:8082  
CMD ["java", "-jar", "/usr/app/servidor-etiqueta-0.1.0.jar"]
