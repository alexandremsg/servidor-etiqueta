package br.jus.trt11.localetiqueta;

import java.io.File;
import java.io.IOException;

public class ProtocoloServico {

	public boolean imprimir(DadosImpressora dados) {

		boolean isImpressoraRetornouSucesso = false;

		try {
			String caminhoTexto = dados.getFile().getAbsolutePath();

			ProcessBuilder pb = null;

			if (isUnix()) {

				String impressoraMapeada = "//" + dados.getNomeEstacao() + "/" + dados.getNomeImpressora();

				pb = new ProcessBuilder("smbclient", impressoraMapeada, "-U", "TRT11/etiqueta%etiqueta", "-c",
						"put \"" + caminhoTexto + "\"");
			} else {
				pb = new ProcessBuilder("cmd", "/c", "type", caminhoTexto,
						">\\\\" + dados.getNomeEstacao() + "\\" + dados.getNomeImpressora());
			}

			Process process = pb.start();

			try {
				isImpressoraRetornouSucesso = process.waitFor() == 0 ? true : false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			deleteFileAssincrono(caminhoTexto);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return isImpressoraRetornouSucesso;
	}

	public static void deleteFileAssincrono(String caminhoArquivoExcluir) {

		File file = new File(caminhoArquivoExcluir);

		new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException ie) {
				}
				file.delete();
			}
		}).start();
	}

	public boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("nix") || os.contains("aix") || os.contains("nux")) {
			return true;
		} else {
			return false;
		}
	}

}
