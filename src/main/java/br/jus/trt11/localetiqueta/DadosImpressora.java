package br.jus.trt11.localetiqueta;

import java.io.File;
import java.io.Serializable;

public class DadosImpressora implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7217934049007628820L;

	private String nomeEstacao;
	private String nomeImpressora;
	private File file;

	public DadosImpressora() {
		super();
	}

	public String getNomeEstacao() {
		return nomeEstacao;
	}

	public void setNomeEstacao(String nomeEstacao) {
		this.nomeEstacao = nomeEstacao;
	}

	public String getNomeImpressora() {
		return nomeImpressora;
	}

	public void setNomeImpressora(String nomeImpressora) {
		this.nomeImpressora = nomeImpressora;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}