package br.jus.trt11.localetiqueta;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.ws.Response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class EtiquetaController {

	@RequestMapping(value = "/protocolo/impressao", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8")
	public void getImprimir(@RequestBody DadosImpressora dados) throws IOException {
		ProtocoloServico protocoloServico = new ProtocoloServico();
		protocoloServico.imprimir(dados);
	}

	@RequestMapping(value = "/print", method = RequestMethod.POST)
	public ResponseEntity<Boolean> imprimir(@RequestParam("body") String dadosImpressoraStr,
			@RequestParam("file") MultipartFile multipartFile) throws IOException {
		DadosImpressora dadosImpressora = new ObjectMapper().readValue(dadosImpressoraStr, DadosImpressora.class);

		File file = new File(multipartFile.getOriginalFilename());

		try (FileOutputStream stream = new FileOutputStream(file)) {
			stream.write(multipartFile.getBytes());
		}

		dadosImpressora.setFile(file);

		ProtocoloServico protocoloServico = new ProtocoloServico();
		boolean sucesso = protocoloServico.imprimir(dadosImpressora);

		if (sucesso) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(true);
		}

	}

}
